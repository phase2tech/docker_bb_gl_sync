FROM python:3.5-alpine

RUN apk --no-cache add git && \
    git --version
